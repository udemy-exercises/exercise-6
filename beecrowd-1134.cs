using System;

class exercise6
{
   static void Main()
   {
      int inputNum = 0;
      int boozeAmount = 0;
      int gasolineAmount = 0;
      int dieselAmount = 0;

      while (inputNum != 4)
      {
         inputNum = int.Parse(Console.ReadLine());

         if (inputNum == 1) {
            boozeAmount++;
         }

         else if (inputNum == 2) {
            gasolineAmount++;
         }

         else if (inputNum == 3) {
            dieselAmount++;
         }

         else if (inputNum == 4) {
            break;
         }
      }

      Console.WriteLine("MUITO OBRIGADO");
      Console.WriteLine($"Alcool: {boozeAmount}");
      Console.WriteLine($"Gasolina: {gasolineAmount}");
      Console.WriteLine($"Diesel: {dieselAmount}");
   }
}
