using System;

class exercise6
{
   static void Main()
   {
      int X = 1;
      int Y = 1;

      while (X != 0 || Y != 0)
      {
         X = int.Parse(Console.ReadLine());
         Y = int.Parse(Console.ReadLine());

         if (X > 0 && Y > 0) {
            Console.WriteLine("primeiro");
         }

         else if (X > 0 && Y < 0) {
            Console.WriteLine("quarto");
         }

         else if (X < 0 && Y > 0) {
            Console.WriteLine("segundo");
         }

         else if (X < 0 && Y < 0) {
            Console.WriteLine("terceiro");
         }

         else {
            break;
         }
      }
   }
}
